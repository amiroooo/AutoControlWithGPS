package Entities;

public class DelayStep extends MapStep{
	long delay;
	public DelayStep(int typeId) {
		super(typeId);
		// TODO Auto-generated constructor stub
	}
	public long getDelay() {
		return delay;
	}
	public void setDelay(long delay) {
		this.delay = delay;
	}
	public DelayStep(long delay) {
		super(2);
		this.delay = delay;
	}
	@Override
	public String toString() {
		return "Delay[" + delay + "]";
	}

}
