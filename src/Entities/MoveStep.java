package Entities;

import application.MapController;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class MoveStep extends MapStep {
	private Double x;
	private Double y;
	private Double z;
	private Circle circle;
	private MapController controller;
	private Integer allowedDistance = 50;
	
	public MoveStep(int typeId) {
		super(typeId);
		// TODO Auto-generated constructor stub
	}
	public Double getX() {
		return x;
	}
	

	public Integer getAllowedDistance() {
		return allowedDistance;
	}
	public void setAllowedDistance(Integer allowedDistance) {
		this.allowedDistance = allowedDistance;
	}
	public Circle getCircle() {
		return circle;
	}
	
	public void setCircle(Circle circle) {
		this.circle = circle;
	}
	// getMapSize() getZoom()
	public Double getRequiredMapPositionX() {
		return (getX() * controller.getZoom()) + controller.getMapSize()/2;
	}	
	
	public Double getRequiredMapPositionY() {
		return ((getY()* controller.getZoom())-controller.getMapSize() +controller.getMapSize()/2)*(-1);
	}
	
	public void setX(Double x) {
		this.x = x;
	}
	public Double getY() {
		return y;
	}
	public void setY(Double y) {
		this.y = y;
	}
	public Double getZ() {
		return z;
	}
	public void setZ(Double z) {
		this.z = z;
	}
	public MoveStep(Double x, Double y, Double z,Integer allowedDistance,MapController controller) {
		super(1);
		this.x = x;
		this.y = y;
		this.z = z;
		this.circle = new Circle(getX(),getY(), 8,Color.GREENYELLOW);
		this.controller = controller;
		this.allowedDistance = allowedDistance;
	}
	@Override
	public String toString() {
		return "Move[x=" + x + ", y=" + y + ", z=" + z + ", delta=" + allowedDistance + "]";
	}
	public void setCircleSelection(boolean b) {
		if (b){
			circle.setFill(Color.MEDIUMORCHID);
		}else
			circle.setFill(Color.GREENYELLOW);
		
	}

}
