package Entities;

public class ArduinoPacket {
	Integer packetId;
	Object data;
	public ArduinoPacket(Integer packetId, Object data) {
		super();
		this.packetId = packetId;
		this.data = data;
	}
	public Integer getPacketId() {
		return packetId;
	}
	public void setPacketId(Integer packetId) {
		this.packetId = packetId;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
	
}
