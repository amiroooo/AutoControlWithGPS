package Entities;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Calendar;
import java.util.HashMap;

public class RawPacket {
	byte[] rawData;
	final long createdAt;
	public RawPacket(byte[] rawData) {
		this.rawData = rawData;
		this.createdAt = Calendar.getInstance().getTimeInMillis();
	}

	public boolean isPacketOfBeaconId(byte beaconId){
		if (getBeaconId() == beaconId){
			return true;
		}
		return false;
	}
	
	
	public byte[] getRawData() {
		return rawData;
	}


	public int getBeaconId() {
		return toInt(rawData,0, 1);
	}


	public int getPacketType() {
		return toInt(rawData,1, 1);
	}

	public int toInt(byte[] arr,int offset, int length) {
	    ByteBuffer bb = ByteBuffer.wrap(arr,offset,length);
	    bb.order(ByteOrder.LITTLE_ENDIAN);
	    if (length==1){
	    	return bb.get();	    
	    }else if (length==2){
	    	return bb.getShort();
	    }else{
	    	return bb.getInt();
	    }
	}
	
	public int toNumber(byte[] arr,int offset) {
	    ByteBuffer bb = ByteBuffer.wrap(arr);
	    bb.order(ByteOrder.LITTLE_ENDIAN);
	   	return bb.getInt(offset);
	}
	
	public int getCodeOfData() {
		return toInt(rawData,2, 2);
	}



	public int getDataSize() {
		return toInt(rawData,4, 1);
	}

	public long getTimeStamp() {
		return toInt(rawData,5, 4);
	}


	public int getCoordinateX() {
		return toNumber(rawData,9);
	}

	public int getCoordinateY() {
		return toNumber(rawData,13);
	}


	public int getCoordinateZ() {
		return toNumber(rawData,17);
	}


	@Override
	public String toString() {
		String result = "Packet: \n";
		result = result +"beacon: " + getBeaconId() + "\n";
		result = result +"getPacketType: " + getPacketType() + "\n";
		result = result +"getCodeOfData: " + getCodeOfData() + "\n";
		result = result +"getDataSize: " + getDataSize() + "\n";
		result = result +"getTimeStamp: " + getTimeStamp() + "\n";
				
		result = result +"X: " + getCoordinateX() + "\n";
		result = result +"Y: " + getCoordinateY() + "\n";
		result = result +"Z: " + getCoordinateZ() + "\n";
		
		return result;
	}
	

	public String toRawString() {
		String result = "Packet: \n";
		for (int i = 0;i<rawData.length;i++){
			result = result +i+": " + rawData[i] + "\n";	
		}
		return result;		
	}
	
	public int[] getLocationOnArray(){
		int[] loc = new int[3];
		loc[0]= getCoordinateX();
		loc[1]= getCoordinateY();
		loc[2]= getCoordinateZ();
		return loc;	
	}
	
	public HashMap<String,Integer> getLocation(){
		HashMap<String,Integer> loc = new HashMap<>();
		loc.put("X", getCoordinateX());
		loc.put("Y", getCoordinateY());
		loc.put("Z", getCoordinateZ());
		return loc;	
	}
	
	public static Double getDirection(RawPacket a,RawPacket b){
		if (a == null || b == null || b.createdAt + 5000 < Calendar.getInstance().getTimeInMillis()){
			return (double) -1;
		}
		Integer i = (a.getCoordinateY()-b.getCoordinateY())/(a.getCoordinateX()-b.getCoordinateX());
		Double result = (Math.atan(i)*180)/Math.PI;
		return result;
	}
	
	public int getHeight(){
		return getCoordinateZ();	
	}
	
	public boolean isResolutionCoordinatePacket(){
		if (getBeaconId() != 0 && getPacketType() == 71 && getDataSize() == 22){
			return true;
		}
		return false;
	}

}
