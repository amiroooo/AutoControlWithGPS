package Entities;

public class MapStep {
	private int typeId;

	public MapStep(int typeId) {
		super();
		this.typeId = typeId;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	
}
