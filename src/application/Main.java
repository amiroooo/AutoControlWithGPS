package application;


import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import Entities.ArduinoPacket;
import Entities.DataBundle;
import Entities.RawPacket;

import javafx.application.Application;
import javafx.application.Platform;


import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

import javafx.scene.Scene;

import javafx.scene.image.Image;
import javafx.scene.layout.Region;



public class Main extends Application{
	
	final public static int UDP_SERVER_PORT = 49100;
	final public static int DELAY_BETWEEN_EACH_DATA_UPDATE_TO_ARDUINO = 400;
	
	public static String USB_PORT = "COM6";
	public static Byte BEACON_ID = 98;
	public static Byte SECONDARY_ID = 90;


	static SerialPort serialPort;
	static Stage primaryStage;
	private static Region root;
	private static Region mapRoot;
	private static IsMainController controller;
	public static DatagramSocket socket;
	static Thread arduinoThread;
	public static RawPacket myBeacon;
	public static RawPacket secondaryBeacon;
	static Thread updateCurrentLocationThread;
	static MapController mapController;


	@Override
	public void start(Stage primaryStage) {
		try {

			Main.primaryStage = primaryStage;
			primaryStage.setTitle("Auto Control With GPS");
			DataBundle dBundle =  new DataBundle();
			dBundle.setData(serialPort);
			primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("resources/appicon.png")));
			root = FXMLLoader.load(getClass().getResource("main_page_layout.fxml"),dBundle);
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);	
			primaryStage.show();
			setupUDPConnectionWithServer();
			openConnectionWithArduino(USB_PORT);

		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static IsMainController getController() {
		return controller;
	}

	public static void setController(IsMainController controller) {
		Main.controller = controller;
	}

	public static void main(String[] args) {
		launch(args);
	}



	public static void openConnectionWithArduino(String port) {
		if (serialPort != null){
			try {
				serialPort.closePort();
				arduinoThread.interrupt();
			} catch (SerialPortException e) {
			}
		}
		serialPort = new SerialPort(port); 
		try {
			serialPort.openPort();//Open port
			getController().setUsbPortText(port);
			getController().setArduinoConnectionStatus(true);
			serialPort.setParams(9600, 8, 1, 0);//Set params
			arduinoThread = new Thread(new Runnable(){

				@Override
				public void run() {
					while(true){
						try {
							String res = serialPort.readString();

							if (res != null && res.length() > 0){
								//serialPort.writeString("!cool!");
								Main.getController().setNewLog(res);
							}
						}
						catch (SerialPortException ex) {
							Main.getController().setNewLog("ERROR: "+ex.getMessage());
						}  

						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}

			});
			arduinoThread.start();
		}
		catch (SerialPortException ex) {
			getController().setArduinoConnectionStatus(false);
		} 
	}

	@Override
	public void stop() throws Exception { 
		if (serialPort != null && serialPort.isOpened()){
			serialPort.closePort();
		}
		System.exit(0);
	}

	public static void sendCurrentLocationToArduino(){
		int[] k = new int[3];
		k[0] = myBeacon.getCoordinateX();
		k[1] = myBeacon.getCoordinateY();
		k[2] = myBeacon.getCoordinateZ();
		ArduinoPacket pck = new ArduinoPacket(1,k);
		Main.sendMessageToArduino(pck);
		updateBeaconLocationOnMap(myBeacon.getCoordinateX(),myBeacon.getCoordinateY(),myBeacon.getCoordinateZ());
	} 

	public static void openMap(){
		Platform.runLater(new Runnable(){
			@Override 
			public void run() {
				try {
					mapController = new MapController();

					FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("map_layout.fxml"));
					fxmlLoader.setController(mapController);
					mapRoot = fxmlLoader.load();

					Scene scene = new Scene(mapRoot);
					Stage mapStage = new Stage();
					mapStage.getIcons().add(new Image(Main.class.getResourceAsStream("resources/appicon.png")));
					mapStage.setTitle("Radar Map");
					mapStage.setScene(scene);
					mapStage.setResizable(false);
					mapStage.show();					
				} catch (IOException e) {
					e.printStackTrace();
				}	
			}
		});  
	}

	public static void updateBeaconLocationOnMap(int x,int y,int z){
		if (mapController != null){
			mapController.updateBeaconLocationOnMap(x,y,z);
		}
	} 


	public static void sendRequiredLocationToArduino(int x,int y,int z){
		int[] k = new int[3];
		k[0] = x;
		k[1] = y;
		k[2] = z;
		ArduinoPacket pck = new ArduinoPacket(3,k);
		Main.sendMessageToArduino(pck);
	} 

	public static void sendDirectionToArduino(){

		Double direction = RawPacket.getDirection(myBeacon, secondaryBeacon);
		Integer directionAngle = direction.intValue();
		int[] k = new int[1];
		k[0] = directionAngle;
		ArduinoPacket pck = new ArduinoPacket(2,k);
		Main.sendMessageToArduino(pck);
	} 	 

	public static void sendMessageToArduino(ArduinoPacket msg){
		if (serialPort != null && serialPort.isOpened()){
			try {
				if (msg.getData() instanceof String){
					serialPort.writeString((String)msg.getData());
				}
				if (msg.getData() instanceof int[]){
					int[] tmp = (int[])msg.getData();
					String stringFromArray = "<" + msg.getPacketId().toString() + ";";
					for(int i=0;i<tmp.length;i++){
						stringFromArray = stringFromArray+tmp[i];
						if (i != tmp.length-1){
							stringFromArray = stringFromArray + ";";
						}
					}
					stringFromArray = stringFromArray + ">";
					serialPort.writeString(stringFromArray);
				}
			} catch (SerialPortException e) {
				e.printStackTrace();
			}
		}
	}

	public void setupUDPConnectionWithServer(){
		//create udp socket connection
		try {
			socket = new DatagramSocket(UDP_SERVER_PORT);
			Thread thread = new Thread(new Runnable(){
				@Override
				public void run() {
					while(true){
						byte[] inData = new byte[30];
						DatagramPacket recievePkt = new DatagramPacket(inData, inData.length);
						try {
							socket.receive(recievePkt);

							RawPacket rp = new RawPacket(recievePkt.getData());
							if (rp.isResolutionCoordinatePacket() && rp.isPacketOfBeaconId(BEACON_ID)){
								Main.getController().setNewLogFromGPS(rp.toString());
								Main.myBeacon = rp;
								if (updateCurrentLocationThread == null){
									updateCurrentLocationThread = new Thread(new Runnable(){

										@Override
										public void run() {
											while(true){
												sendCurrentLocationToArduino();

												try {
													Thread.sleep(DELAY_BETWEEN_EACH_DATA_UPDATE_TO_ARDUINO);
												} catch (InterruptedException e) {
													e.printStackTrace();
												}
												sendDirectionToArduino();
												try {
													Thread.sleep(DELAY_BETWEEN_EACH_DATA_UPDATE_TO_ARDUINO);
												} catch (InterruptedException e) {
													e.printStackTrace();
												}	
											}
										}


									}); 
									updateCurrentLocationThread.start();
								}
							}

							if (rp.isResolutionCoordinatePacket() && rp.isPacketOfBeaconId(SECONDARY_ID)){
								Main.secondaryBeacon = rp;
							}				    	  

							getController().setUdpConnectionStatus(true);
						} catch (IOException e1) {
							getController().setUdpConnectionStatus(false);
						}
					}
				}

			});
			thread.start();
		} catch (SocketException e) {

		}
	}

	public static void connectToArduino(String port) {
		openConnectionWithArduino(port);
	}

	public static String[] getUsbPortsOnPc(){
		String[] portNames = SerialPortList.getPortNames();
		return portNames;
	}

}
