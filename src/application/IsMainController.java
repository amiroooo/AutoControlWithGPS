package application;

public interface IsMainController {
	public void setNewLog(String msg);
	public void setNewLogFromGPS(String msg);
	public void setUdpConnectionStatus(boolean isOn);
	public void setArduinoConnectionStatus(boolean isOn);
	public void setUsbPortText(String port);
}
