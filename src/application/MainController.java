package application;

import java.net.URL;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

import Entities.ArduinoPacket;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.paint.Color;


public class MainController implements Initializable,IsMainController {

	@FXML
	TextArea loggerTextArea;
	@FXML
	TextField msgToSend;
	@FXML
	TextArea gpsLoggerTextArea;
	@FXML
	Label udpConnectionStatusLabel;

	@FXML
	Label arduinoConnectionStatusLabel1;	
	@FXML
	TitledPane tiledPane1;
	@FXML
	TitledPane tiledPane2;
	
	@FXML
	TextField mainBeacon;	
	@FXML
	TextField secondaryBeacon;	
	
	@FXML
	Label currentSecondary;
	@FXML
	Label currentMain;
	
	
	
	
	@FXML
	Spinner<String> portSpinner;
	ObservableList<String> resObsList;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Main.setController(this);
		String[] ports = Main.getUsbPortsOnPc();
		resObsList = FXCollections.observableArrayList(ports);
		SpinnerValueFactory<String> valueFactory = new SpinnerValueFactory.ListSpinnerValueFactory(resObsList);
		portSpinner.setValueFactory(valueFactory);
		loadData();

		tiledPane1.heightProperty().addListener((obs, oldHeight, newHeight) -> Main.primaryStage.sizeToScene());
		tiledPane2.heightProperty().addListener((obs, oldHeight, newHeight) -> Main.primaryStage.sizeToScene());
	}

	private void updateBeaconIdsOnGui(){
		currentSecondary.setText(Main.SECONDARY_ID.toString());
		currentMain.setText(Main.BEACON_ID.toString());
		saveData();
	}
	
	@FXML
	public void onClickUpdateBeacons(){
		Byte mainBeaconByte = null;
		Byte secondaryBeaconByte = null;
		if (!secondaryBeacon.getText().isEmpty()){
			try{
				secondaryBeaconByte = Byte.parseByte(secondaryBeacon.getText());
				Main.SECONDARY_ID = secondaryBeaconByte;
			}catch(NumberFormatException e){
				System.out.println(e.getMessage());
			}
		}
		try{
			mainBeaconByte = Byte.parseByte(mainBeacon.getText());
			Main.BEACON_ID = mainBeaconByte;
			updateBeaconIdsOnGui();	
		}catch(NumberFormatException e){
			System.out.println(e.getMessage());
		}	
	}

	
	public void saveData(){
		Preferences prefs = Preferences.userNodeForPackage(Main.class);
	    prefs.putInt("beacon_id", Main.BEACON_ID);
	    prefs.putInt("secondary_id", Main.SECONDARY_ID);
	    prefs.put("arduino_usb_port", Main.USB_PORT); 
	}
	
	public void loadData(){
	    Preferences prefs = Preferences.userNodeForPackage(Main.class);
	    Integer tempBeaconId =  prefs.getInt("beacon_id", 98);
	    Integer tempSecondaryId =  prefs.getInt("secondary_id", 98);
	    String tempArduinoUsbPortId =  prefs.get("arduino_usb_port", "COM6");
	    
	   try {
		   Main.BEACON_ID = Byte.parseByte(tempBeaconId.toString());
		   Main.SECONDARY_ID = Byte.parseByte(tempSecondaryId.toString());
		   Main.USB_PORT = tempArduinoUsbPortId;
	   }catch(NumberFormatException e){
			System.out.println(e.getMessage());
	   }	
	   updateBeaconIdsOnGui();
	}	
	
	/*
	 * Here you handle messages arriving from Arduino
	 */
	@Override
	public void setNewLog(String msg) {
		Platform.runLater(new Runnable(){
			@Override 
			public void run() {
				loggerTextArea.setText(msg);
			}
		});
	}


	@FXML
	public void onClickSend(){
		int[] k = new int[3];
		k[0] =50;
		k[1] =100;
		k[2] =150;
		ArduinoPacket pck = new ArduinoPacket(1,k);
		Main.sendMessageToArduino(pck);
	}

	/*
	 * Here you handle messages arriving from GPS Router
	 */
	@Override
	public void setNewLogFromGPS(String msg) {
		Platform.runLater(new Runnable(){
			@Override 
			public void run() {
				gpsLoggerTextArea.setText(msg);	
			}
		});
	}

	@FXML
	public void onClickConnectUsbPort(){
		Main.connectToArduino(portSpinner.getValue().toString());
	}


	public void setUdpConnectionStatus(boolean isOn){
		Platform.runLater(new Runnable(){
			@Override 
			public void run() {
				if (isOn){
					udpConnectionStatusLabel.setText("Connected");
					udpConnectionStatusLabel.setTextFill(Color.web("#00FF00"));
				}else{
					udpConnectionStatusLabel.setText("Not Established");
					udpConnectionStatusLabel.setTextFill(Color.web("#FF0000"));
				}
			}
		});
	}

	public void setArduinoConnectionStatus(boolean isOn){
		Platform.runLater(new Runnable(){
			@Override 
			public void run() {
				if (isOn){
					arduinoConnectionStatusLabel1.setText("Connected");
					arduinoConnectionStatusLabel1.setTextFill(Color.web("#00FF00"));
					saveData();
				}else{
					arduinoConnectionStatusLabel1.setText("Not Established");
					arduinoConnectionStatusLabel1.setTextFill(Color.web("#FF0000"));
				}
			}
		});
	}	


	@FXML
	public void onOpenMapClick(){
		Main.openMap();		
	}



	@Override
	public void setUsbPortText(String port) {

		for (Iterator<String> iterator = resObsList.iterator(); iterator.hasNext(); ) {
			String value = iterator.next();
			if (value.equals(port)){
				iterator.remove();
				resObsList.add(0, value);
				break;
			}
		}
		SpinnerValueFactory<String> valueFactory = new SpinnerValueFactory.ListSpinnerValueFactory(resObsList);
		Platform.runLater(new Runnable(){
			@Override 
			public void run() {
				portSpinner.setValueFactory(valueFactory);
			}
		});
	}
}
