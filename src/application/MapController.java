package application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Entities.DelayStep;
import Entities.MapStep;
import Entities.MoveStep;
import application.DragResizeMod.OnDragResizeEventListener;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

public class MapController implements Initializable {
	private static final Duration TRANSLATE_DURATION = Duration.seconds(1.25);
	private static final float ZOOM_BASE_MULTIPLIER = (float) 10.0;
	public static final int IN_ZONE_RADIUS = 35;
	private static double DEFAULT_MAP_SIZE = 2000;
	private double MapSize = DEFAULT_MAP_SIZE;
	private int leftPaneSize;
	@FXML
	Label zoomValueLabel;
	@FXML
	Slider heightSlider;
	@FXML 
	CheckBox repeatCheckBox;
	@FXML
	TextField delayTextField;
	@FXML
	TextField xTextField;
	
	@FXML
	TextField allowedDistanceTextField;	
	
	@FXML
	TextField yTextField;
	@FXML
	TextField zTextField;
	@FXML
	Circle circle;
	@FXML
	Circle selectionCircle;	

	@FXML
	AnchorPane rightPane;		
	@FXML
	SplitPane splitPane;
	@FXML
	Pane pane;
	@FXML
	ListView<MapStep> stepListView;
	@FXML
	ScrollPane scrollPane;

	ArrayList<MapStep> planList;
	
	@FXML
	TextField mapAngleTextField;
	@FXML
	TextField mapWidth;	
	@FXML
	TextField mapHeight;
	@FXML
	TextField mapX;	
	@FXML
	TextField mapY;		
	
	
	@FXML
	Separator verticalSeparator;
	@FXML
	Separator horizontalSeparator;
	@FXML
	ImageView mapImageView;
	
	String mapPath;
	
	TranslateTransition transitionCircle;
	TranslateTransition transitionSelectionCircle;
	private Float zoom;
	private boolean isRepeat;

	Thread planExecutionEventThread;
	private int beaconX;
	private int beaconY;
	private int beaconZ;
	
	public double getMapSize() {
		return MapSize;
	}

	public void setMapSize(double mapSize) {
		MapSize = mapSize;
	}

	public Float getZoom() {
		return (float) 1.0;
	}
	public Float getScale() {
		return zoom;
	}	

	public void setZoom(Float zoom) {
		this.zoom = zoom;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		DEFAULT_MAP_SIZE = pane.getPrefHeight();
		leftPaneSize = (int) (splitPane.getWidth() - rightPane.getWidth());
		zoom = (float) 1;
		isRepeat = false;
		transitionCircle = createTranslateTransition(circle);	
		transitionSelectionCircle = createTranslateTransition(selectionCircle);
		stepListView.setCellFactory(param -> new ListCell<MapStep>() {
			@Override
			protected void updateItem(MapStep item, boolean empty) {
				super.updateItem(item, empty);
				Platform.runLater(new Runnable(){
					@Override 
					public void run() {
						if (item != null && (item instanceof MapStep)){
							setText(item.toString());
						}else
							setText("");
					}
				});
			}
		});	
		
		stepListView.setOnKeyPressed(new EventHandler<KeyEvent>(){
			@Override
			public void handle(KeyEvent event) {
				if ((event.getCode() == KeyCode.BACK_SPACE) || (event.getCode() == KeyCode.DELETE)){
					if (!stepListView.getSelectionModel().isEmpty() && stepListView.getSelectionModel().getSelectedIndex()>-1)
					{
						int index = stepListView.getSelectionModel().getSelectedIndex();
						deleteMapStepFromArray(index);
					}
					
				}
			}
			
			
		});
		stepListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<MapStep>() {

		    @Override
		    public void changed(ObservableValue<? extends MapStep> observable, MapStep oldValue, MapStep newValue) {
		    	if (oldValue instanceof MoveStep){
		    		MoveStep moveStep = (MoveStep)oldValue;
		    		moveStep.setCircleSelection(false);
		    	}
		    	if (newValue instanceof MoveStep){
		    		MoveStep moveStep = (MoveStep)newValue;
		    		moveStep.setCircleSelection(true);
		    	}
		    }
		});
		planList = new ArrayList<>();
		scrollPane.setVvalue(0.5);
		scrollPane.setHvalue(0.5);
		setOnMouseClick();
		setMapImageDragSettings();
	}
	
	@FXML
	public void onClickPlusZoom(){
		if (zoom < 16){
			zoom = (float) (zoom*2);
			Platform.runLater(new Runnable(){
				@Override 
				public void run() {
					zoomValueLabel.setText(zoom.toString()+"x");
					resizePane();
					updateMoveCirclesAndSelectionCircle();
				}
			});
		}
	}

    private void setNodeSize(Node node, double x, double y, double h, double w) {
        node.setLayoutX(x);
        node.setLayoutY(y);
        if (node instanceof ImageView) {
        	ImageView tempImage = (ImageView) node;
        	System.out.println("height: "+h);
        	tempImage.setFitHeight(h);
        	tempImage.setFitWidth(w);
        }
    }
    
    private void setNodeCoordinates(Node node, double x, double y) {
        node.setLayoutX(x);
        node.setLayoutY(y);
    }   
    
	private void setMapImageDragSettings(){
		DragResizeMod.makeResizable(mapImageView,new OnDragResizeEventListener(){

			@Override
			public void onDrag(Node node, double x, double y, double h, double w) {
				setNodeCoordinates(node,x, y);
				mapX.setText(((Double)x).toString());
				mapY.setText(((Double)y).toString());
				mapWidth.setText(((Double)w).toString());
				mapHeight.setText(((Double)h).toString());
			}

			@Override
			public void onResize(Node node, double x, double y, double h, double w) {
				setNodeSize(node, x, y, h, w);
				mapX.setText(((Double)x).toString());
				mapY.setText(((Double)y).toString());
				mapWidth.setText(((Double)w).toString());
				mapHeight.setText(((Double)h).toString());
			}
		});
	}	
	
	@FXML
	public void onClickAdjustMap(){
		Double angle = null;
		Double width = null;
		Double height = null;
		Double tempMapX = null;
		Double tempMapY = null;
		try{
			width = Double.parseDouble(mapWidth.getText());
			height = Double.parseDouble(mapHeight.getText());
			tempMapX = Double.parseDouble(mapX.getText());
			tempMapY = Double.parseDouble(mapY.getText());
			mapImageView.setFitHeight(height);
			mapImageView.setFitWidth(width);
			mapImageView.setLayoutX(tempMapX);
			mapImageView.setLayoutY(tempMapY);
		}catch(NumberFormatException e){
			System.out.println(e.getMessage());
		}
		try{
			angle = Double.parseDouble(mapAngleTextField.getText());
			mapImageView.setRotate(angle);
		}catch(NumberFormatException e){
			angle = 0.0;
			mapImageView.setRotate(angle);
			mapAngleTextField.setText("0");
		}
	}
	
	@FXML
	public void onClickLoadMap(){
        FileChooser fileChooser = new FileChooser();
        
        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Image Files", "*.jpg","*.png","*.bmp");
        fileChooser.getExtensionFilters().add(extFilter);
         
        //Show save file dialog
        Stage stage = (Stage) splitPane.getScene().getWindow();
        File file = fileChooser.showOpenDialog(stage);
        if(file != null){
        	try {
				mapPath = file.toURI().toURL().toString();
	    		Platform.runLater(new Runnable(){
	    			@Override 
	    			public void run() {	
	    				Image img = new Image(mapPath);
	    				mapImageView.setImage(img);
	        			mapAngleTextField.setText("0");
	        			mapWidth.setText(((Double)mapImageView.getFitWidth()).toString());
	        			mapHeight.setText(((Double)mapImageView.getFitHeight()).toString());
	        			mapX.setText(((Double)mapImageView.getLayoutX()).toString());
	        			mapY.setText(((Double)mapImageView.getLayoutY()).toString());
	    			}
	    		});
				
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

        }else{
    		Platform.runLater(new Runnable(){
    			@Override 
    			public void run() {	
    				mapImageView.setImage(null);
    			}
    		});
        }
	}	
	
	
	
	private void resizePane(){
		Platform.runLater(new Runnable(){
			@Override 
			public void run() {			
				pane.setScaleX(getScale());
				pane.setScaleY(getScale());
				verticalSeparator.resizeRelocate(verticalSeparator.getLayoutX(), 30, verticalSeparator.getWidth(), DEFAULT_MAP_SIZE*getScale());
				verticalSeparator.setScaleX(1.0/getScale());
				verticalSeparator.setScaleY(1.0/getScale());	
				horizontalSeparator.setScaleX(1.0/getScale());
				horizontalSeparator.setScaleY(1.0/getScale());
				horizontalSeparator.resizeRelocate(30, horizontalSeparator.getLayoutY(), DEFAULT_MAP_SIZE*getScale(), horizontalSeparator.getHeight());

			}
		});
	}
	
	public void updateMoveCirclesAndSelectionCircle(){
		
		Platform.runLater(new Runnable(){
			@Override 
			public void run() {	
				for (int i=pane.getChildren().size();i>3;i--){
					pane.getChildren().remove(i-1);
					
				}
				pane.getChildren().add(circle);
				pane.getChildren().add(selectionCircle);
				circle.setScaleX(1.0/getScale());
				circle.setScaleY(1.0/getScale());
				selectionCircle.setScaleX(1.0/getScale());
				selectionCircle.setScaleY(1.0/getScale());
				for(int i =0; i<planList.size();i++){
					if (planList.get(i).getTypeId() == 1){
						MoveStep moveStep = (MoveStep)planList.get(i);
						moveStep.getCircle().setScaleX(1.0/getScale());
						moveStep.getCircle().setScaleY(1.0/getScale());
						moveStep.getCircle().setCenterX(moveStep.getRequiredMapPositionX());
						moveStep.getCircle().setCenterY(moveStep.getRequiredMapPositionY());
						pane.getChildren().add(moveStep.getCircle());
					}
				}
				
			}
		});	
		
	}
	
	@FXML
	public void onClickMinusZoom(){
		if (zoom > 0.01){
			zoom = (float) (zoom/2);
			Platform.runLater(new Runnable(){
				@Override 
				public void run() {
					zoomValueLabel.setText(zoom.toString()+"x");
					resizePane();
					updateMoveCirclesAndSelectionCircle();
				}
			});
		}
	}
	
	
	@FXML
	public void onClickLoad(){
        FileChooser fileChooser = new FileChooser();
        
        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("All files", "*.*");
        fileChooser.getExtensionFilters().add(extFilter);
         
        //Show save file dialog
        Stage stage = (Stage) splitPane.getScene().getWindow();
        File file = fileChooser.showOpenDialog(stage);
        if(file != null){
            ArrayList<String> fileData = readFile(file);
            for (int i = 0;i< fileData.size();i++){
				String[] arr = (fileData.get(i)).split(","); 
				addStepFromFileData(arr);           			
            }
        }
	}
	
    private void addStepFromFileData(String[] arr) {
		// TODO Auto-generated method stub
    	if (arr != null && arr.length>0){
    		if (arr[0].equals("1")){
    			addMoveStepFromFileData(arr[1],arr[2],arr[3],arr[4]);
    		}else if (arr[0].equals("2")){
    			addDelayStepFromFileData(arr[1]);
    		}else if (arr[0].equals("3")){
    			addImageStepFromFileData(arr[1],arr[2],arr[3],arr[4],arr[5],arr[6]);
    		}
    	}	
	}

	private void addImageStepFromFileData(String string,String string1, String string2, String string3, String string4,
			String string5) {

		Double angle = null;
		Double width = null;
		Double height = null;
		Double tempMapX = null;
		Double tempMapY = null;
		try{
			
			width = Double.parseDouble(string3);
			height = Double.parseDouble(string4);
			tempMapX = Double.parseDouble(string1);
			tempMapY = Double.parseDouble(string2);
			angle = Double.parseDouble(string5);
			
			final Double fHeight = height;
			final Double fWidth = width;
			final Double fTempMapX = tempMapX;
			final Double ftempMapY = tempMapY;
			final Double fAngle = angle;
			
    		Platform.runLater(new Runnable(){
    			@Override 
    			public void run() {	
					mapImageView.setImage(new Image(string));
					mapImageView.setFitHeight(fHeight);
					mapImageView.setFitWidth(fWidth);
					mapImageView.setLayoutX(fTempMapX);
					mapImageView.setLayoutY(ftempMapY);
					mapImageView.setRotate(fAngle);
        			mapAngleTextField.setText(fAngle.toString());
        			mapWidth.setText(fWidth.toString());
        			mapHeight.setText(fHeight.toString());
        			mapX.setText(fTempMapX.toString());
        			mapY.setText(ftempMapY.toString());
				}
    		});
		}catch(NumberFormatException e){
			System.out.println(e.getMessage());
		}
	}

	private ArrayList<String> readFile(File file){
        ArrayList<String> stringList = new ArrayList<>();
        BufferedReader bufferedReader = null;
         
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            String text;
            while ((text = bufferedReader.readLine()) != null) {
            	stringList.add(text);
            }
 
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {

        } finally {
            try {
                bufferedReader.close();
            } catch (IOException ex) {

            }
        }
        return stringList;
    }
    
	@FXML
	public void onClickSave(){	
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Plan");
        System.out.println("Saving plan");
        Stage stage = (Stage) splitPane.getScene().getWindow();
        File file = fileChooser.showSaveDialog(stage);
        if (file != null) {
            saveFile(getListData(), file);
        }
	}
	
    private void saveFile(String content, File file){
        try {
            FileWriter fileWriter = null;
             
            fileWriter = new FileWriter(file);
            fileWriter.write(content);
            fileWriter.close();
        } catch (IOException ex) {

        }    
    }
    
    private String getListData(){
    	String str = "";
    	if (mapPath != null){
    		Double angle = null;
    		Double width = null;
    		Double height = null;
    		Double tempMapX = null;
    		Double tempMapY = null;
    		try{
    			angle = Double.parseDouble(mapAngleTextField.getText());
    		}catch(NumberFormatException e){
    			angle = 0.0;
    		}  
    		try{
    			width = Double.parseDouble(mapWidth.getText());
    			height = Double.parseDouble(mapHeight.getText());
    			tempMapX = Double.parseDouble(mapX.getText());
    			tempMapY = Double.parseDouble(mapY.getText());
    			str = str + "3," + mapPath + ", " + tempMapX + ", " +tempMapY + ", " + width + ", " + height + ", " + angle + "\n";
    		}catch(NumberFormatException e){
    			System.out.println(e.getMessage());
    		}
    	}
    	for (int i = 0; i<planList.size();i++){
    		if (planList.get(i).getTypeId() == 1){
    			MoveStep moveStep = (MoveStep)planList.get(i);
    			str = str + moveStep.getTypeId() + "," + moveStep.getX() + "," + moveStep.getY() + "," + moveStep.getZ();
    			str = str + "," + moveStep.getAllowedDistance().toString();
    			str = str + "\n";
    		}else if (planList.get(i).getTypeId() == 2){
    			DelayStep delayStep = (DelayStep)planList.get(i);
    			str = str + delayStep.getTypeId() + "," + delayStep.getDelay()+"\n";   			
    		}	
    	}
    	return str;
    }
    
	@FXML
	public void onClearClick(){	
		if (planExecutionEventThread != null){
			planExecutionEventThread.interrupt();
			planExecutionEventThread.stop();
			planExecutionEventThread = null;
		}
		planList = new ArrayList<>();
		updateListItems();
		updateMoveCirclesAndSelectionCircle();
	}	


	@FXML
	public void onClickAddStep(){
		Double x = null;
		Double y = null;
		Double z = null;
		Integer allowedDistance = null;
		try{
			x = Double.parseDouble(xTextField.getText());
			y = Double.parseDouble(yTextField.getText());
			z = Double.parseDouble(zTextField.getText());
			allowedDistance = Integer.parseInt(allowedDistanceTextField.getText());
		}catch(NumberFormatException e){
			System.out.println(e.getMessage());
		}
		if (x != null && z != null && y != null){
			MoveStep moveStep = new MoveStep(x,y,z,allowedDistance,this);
			addMapStepToArray(moveStep);
			updateListItems();
			updateMoveCirclesAndSelectionCircle();
		}
	}
	

	private void addMapStepToArray(MapStep mapstep){
		if (planList.size() == 0){
			planList.add(0, mapstep);
		}else{
			if (stepListView.getSelectionModel().isEmpty()){
				planList.add(mapstep);
			}else
				planList.add(stepListView.getSelectionModel().getSelectedIndex()+1, mapstep);
		}		
	}

	
	private void deleteMapStepFromArray(int index){
		planList.remove(index);
		updateListItems();
		updateMoveCirclesAndSelectionCircle();		
	}	

	@FXML
	public void onClickAddDelay(){
		Long t = null;
		try{		
			t = Long.parseLong(delayTextField.getText());
		}catch(NumberFormatException e){
			System.out.println(e.getMessage());
		}		
		if (t != null){
			DelayStep delayStep = new DelayStep(t);
			addMapStepToArray(delayStep);
			updateListItems();
		}
	}

	@FXML
	public void onClickRepeatCheckBox(){
		isRepeat = repeatCheckBox.isSelected();
	}

	@FXML
	public void onClickStartPlan(){
		System.out.println("Starting plan!");
		if (planExecutionEventThread != null){
			planExecutionEventThread.interrupt();
			planExecutionEventThread.stop();
		}
		
		planExecutionEventThread = new Thread(new Runnable(){

			@Override
			public void run() {
				boolean planInProgress = true;
				while(planInProgress){
					//planList
					int i = 0;
					if (planList.size() == 0){
						System.out.println("no items to execute");
					}else{
						if (stepListView.getSelectionModel().isEmpty()){
							i = 0;
						}else{
							i = stepListView.getSelectionModel().getSelectedIndex();
						}
						switch (planList.get(i).getTypeId()){
							case 1:{
								MoveStep moveStep = (MoveStep)planList.get(i);
								if (isMovePointCloseToCircle(moveStep)){
									int nextI = stepListView.getSelectionModel().getSelectedIndex()+1;
									if (planList.size()>nextI){
										stepListView.getSelectionModel().select(nextI);
									}else if(isRepeat){
										stepListView.getSelectionModel().select(0);
									}else{
										System.out.println("Plan finished executing.");
										planInProgress = false;
									}
								}else{
									
									int multiplierInInt = (int) ZOOM_BASE_MULTIPLIER;
									int x = moveStep.getX().intValue();
									int y = moveStep.getY().intValue();
									int z = moveStep.getZ().intValue();
									Main.sendRequiredLocationToArduino(x*multiplierInInt, y*multiplierInInt, z*multiplierInInt);
									try {
										Thread.sleep(250);	
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								break;
							}
							case 2:{
								DelayStep delayStep = (DelayStep)planList.get(i);
								try {
									Thread.sleep(delayStep.getDelay());
									int nextI = stepListView.getSelectionModel().getSelectedIndex()+1;
									if (planList.size()>nextI){
										stepListView.getSelectionModel().select(nextI);
									}else if(isRepeat){
										stepListView.getSelectionModel().select(0);
									}else{
										System.out.println("Plan finished executing.");
										planInProgress = false;
										break;
									}
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								break;
							}
						}
						
					}	
					
				}
			}

			private boolean isMovePointCloseToCircle(MoveStep moveStep) {
				double difX = beaconX/ZOOM_BASE_MULTIPLIER-moveStep.getX();
				double difY = beaconY/ZOOM_BASE_MULTIPLIER-moveStep.getY();
				double difZ = beaconZ/ZOOM_BASE_MULTIPLIER-moveStep.getZ();
				double distance = Math.sqrt(difX*difX +difY*difY);
				if (distance <= moveStep.getAllowedDistance() && difZ < moveStep.getAllowedDistance()){
					return true;
				}else{
					return false;
				}
			}
		});
		planExecutionEventThread.start();
		
		

	}

	private TranslateTransition createTranslateTransition(final Circle circle) {
		final TranslateTransition transition = new TranslateTransition(TRANSLATE_DURATION, circle);
		transition.setOnFinished(new EventHandler<ActionEvent>() {
			@Override public void handle(ActionEvent t) {
				circle.setCenterX(circle.getTranslateX() + circle.getCenterX());
				circle.setCenterY(circle.getTranslateY() + circle.getCenterY());
				circle.setTranslateX(0);
				circle.setTranslateY(0);
			}
		});
		return transition;
	}


	public void updateBeaconLocationOnMap(int x,int y,int z){
		beaconX = x;
		beaconY = y;
		beaconZ = z;
		Platform.runLater(new Runnable(){
			@Override 
			public void run() {
				if (transitionCircle != null){
					double resX = ((x/ZOOM_BASE_MULTIPLIER) * getZoom()) + getMapSize()/2;
					double resY = (((y/ZOOM_BASE_MULTIPLIER)* getZoom()) - getMapSize() + getMapSize()/2)*(-1);
					if (!circle.isVisible()){
						circle.setVisible(true);
					}
					transitionCircle.setToX(resX);
					transitionCircle.setToY(resY);//newY - circle.getCenterY()
					transitionCircle.playFromStart();
					//pane.getChildren().add(transitionCircle);
				}
			}
		});
	}


	
	private void addMoveStepFromFileData(String stringX,String stringY,String stringZ,String allowed){
		
		Double x = null;
		Double y = null;
		Double z = null;
		Integer allowedDistance = null;
		try{
			x = Double.parseDouble(stringX);
			y = Double.parseDouble(stringY);
			z = Double.parseDouble(stringZ);
			allowedDistance = Integer.parseInt(allowed);
		}catch(NumberFormatException e){
			System.out.println(e.getMessage());
		}
		if (x != null && z != null && y != null && allowedDistance != null){
			MoveStep moveStep = new MoveStep(x,y,z,allowedDistance,this);
			addMapStepToArray(moveStep);
			updateListItems();
			updateMoveCirclesAndSelectionCircle();
		}
	}
	/*
	private void addMoveStepFromFileData(String stringX,String stringY,String stringZ){
		
		Double x = null;
		Double y = null;
		Double z = null;
		try{
			x = Double.parseDouble(stringX);
			y = Double.parseDouble(stringY);
			z = Double.parseDouble(stringZ);
		}catch(NumberFormatException e){
			System.out.println(e.getMessage());
		}
		if (x != null && z != null && y != null){
			MoveStep moveStep = new MoveStep(x,y,z,this);
			addMapStepToArray(moveStep);
			updateListItems();
			updateMoveCirclesAndSelectionCircle();
		}
	}
	*/
	private void addDelayStepFromFileData(String delay){
		Long t = null;
		try{		
			t = Long.parseLong(delay);
		}catch(NumberFormatException e){
			System.out.println(e.getMessage());
		}		
		if (t != null){
			DelayStep delayStep = new DelayStep(t);
			addMapStepToArray(delayStep);
			updateListItems();
		}
	}	

	private void updateListItems(){
		ObservableList<MapStep> items = FXCollections.observableArrayList(planList);
		Platform.runLater(new Runnable(){
			@Override 
			public void run() {
				
				stepListView.getItems().clear();
				stepListView.setItems(items);
				stepListView.refresh();
			}
		});
	}
	
	
	public void setOnMouseClick(){
		horizontalSeparator.setMouseTransparent(true);
		verticalSeparator.setMouseTransparent(true);
		
		pane.setOnMousePressed(new EventHandler<MouseEvent>() {
	    	@Override 
	    	public void handle(MouseEvent event) {
	    		Platform.runLater(new Runnable(){
	    			@Override 
	    			public void run() {
	    				selectionCircle.setScaleX(1.0/getScale());
	    				selectionCircle.setScaleY(1.0/getScale());
	    				selectionCircle.setCenterX(event.getX());
	    				selectionCircle.setCenterY(event.getY());
			    		if (!selectionCircle.isVisible()){
			    			selectionCircle.setVisible(true);
			    		}
			    		Double x = (event.getX()-DEFAULT_MAP_SIZE/2)/getZoom();
			    		Double y = (DEFAULT_MAP_SIZE-event.getY()-DEFAULT_MAP_SIZE/2)/getZoom();
			    		Double z = heightSlider.getValue();
			    		xTextField.setText(x.toString());
			    		yTextField.setText(y.toString());
			    		String zString = z.toString();
			    		if (zString.length()>6){
			    			zString = zString.substring(0, 6);
			    		}
			    		zTextField.setText(zString);
		    		}
	    		});
		    }

		});
		
	}
}
